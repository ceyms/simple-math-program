/**
*
* @author Cem Arguvanlı
* @since 2014-12-13
*
*/
var entryNumberOne = getElementById("entryNumberOne");
var entryNumberTwo = getElementById("entryNumberTwo");

var resultCollection = getElementById("resultCollection");
var resultExtraction = getElementById("resultExtraction");
var resultMultiplication = getElementById("resultMultiplication");
var resultDivision = getElementById("resultDivision");
var resultFactorial = getElementById("resultFactorial");

var mainResult = getElementById("mainResult");

function getElementById(id) {
    return document.getElementById(id);
};

function showMessage(text) {
    var mainResult = getElementById("mainResult");
    mainResult.innerHTML = text;
};

function isNumberOlder(val) {
    if (typeof val === "number") {
        return true;
    } else {
        return false;
    }
}

function isNumber(val) {
    if (parseInt(val) - parseInt(val) == 0) {
        return true;
    }
    return false;
}

// toplama
function getCollection(firstNumber, secondNumber) {
    mainResult.innerHTML = "";
    var firstNumber = entryNumberOne.value;
    var secondNumber = entryNumberTwo.value;
    if (firstNumber == "" || secondNumber == "") {
        showMessage("Lütfen tüm alanı doldurunuz");
        return false;
    } else {
        if (!isNumber(firstNumber) || !isNumber(secondNumber)) {
            showMessage("Sadece rakam girebilirsiniz");
            return false;
        }
        var result = parseInt(firstNumber) + parseInt(secondNumber);
        showMessage("Toplamanın sonucu: " + result);
        return true;
    }
};

// çıkarma
function getExtraction(firstNumber, secondNumber) {
    mainResult.innerHTML = "";
    var firstNumber = entryNumberOne.value;
    var secondNumber = entryNumberTwo.value;
    if (firstNumber == "" || secondNumber == "") {
        showMessage("Lütfen tüm alanı doldurunuz");
        return false;
    } else {
        if (!isNumber(firstNumber) || !isNumber(secondNumber)) {
            showMessage("Sadece rakam girebilirsiniz");
            return false;
        }
        var result = parseInt(firstNumber) - parseInt(secondNumber);
        showMessage("Çıkarmanın sonucu: " + result);
        return true;
    }
}



// çarpma
function getMultiplication(firstNumber, secondNumber) {
    mainResult.innerHTML = "";
    var firstNumber = entryNumberOne.value;
    var secondNumber = entryNumberTwo.value;
    if (firstNumber == "" || secondNumber == "") {
        showMessage("Lütfen tüm alanı doldurunuz");
        return false;
    } else {
        if (!isNumber(firstNumber) || !isNumber(secondNumber)) {
            showMessage("Sadece rakam girebilirsiniz");
            return false;
        }
        var result = parseInt(firstNumber) * parseInt(secondNumber);
        showMessage("Çarpmanın sonucu: " + result);
    }
};

// bölme
function getDivision(firstNumber, secondNumber) {
    mainResult.innerHTML = "";
    var firstNumber = entryNumberOne.value;
    var secondNumber = entryNumberTwo.value;
    if (firstNumber == "" && secondNumber == "") {
        showMessage("Lütfen tüm alanı doldurunuz");
        return false;
    } else {
        if (!isNumber(firstNumber) || !isNumber(secondNumber)) {
            showMessage("Sadece rakam girebilirsiniz");
            return false;
        }
        var result = parseInt(firstNumber) / parseInt(secondNumber);
        showMessage("Bölmenin sonucu: " + result);
    }
}

function bindEvents() {
    resultCollection.addEventListener('click', getCollection);
    resultExtraction.addEventListener('click', getExtraction);
    resultMultiplication.addEventListener('click', getMultiplication);
    resultDivision.addEventListener('click', getDivision);
};

bindEvents();






/*
function getResult(process, firstNumber, secondNumber) {
    if (entryNumberOneValue == "" && entryNumberTwoValue == "") {
        alert("Bir rakam giriniz");
        return false;
    }

    var entryNumberOneValue = entryNumberOne.value;
    var entryNumberTwoValue = entryNumberTwo.value;

    if (process == "collection") {
        var result = parseInt(entryNumberOneValue) + parseInt(entryNumberTwoValue);
        mainResult.innerHTML += "Toplama Sonucunuz " + result;
        return result;
    }

    if (process == "extraction") {
        var result = (entryNumberOneValue) - (entryNumberTwoValue);
        mainResult.innerHTML += "Çıkarma Sonucunuz " + result;
        return result;
    }

    if (process == "multiplication") {
        var result = (entryNumberOneValue) * (entryNumberTwoValue);
        mainResult.innerHTML += "Çarpma Sonucunuz " + result;
        return result;
    }

    if (process == "division") {
        var result = (entryNumberOneValue) / (entryNumberTwoValue);
        mainResult.innerHTML += "Bölme Sonucunuz " + result;
        return result;
    }

}
resultCollection.addEventListener('click', function() {
    getResult('collection')
});

resultExtraction.addEventListener('click', function() {
    getResult('extraction')
});

resultMultiplication.addEventListener('click', function() {
    getResult('multiplication')
});

resultDivision.addEventListener('click', function() {
    getResult('division')
});

*/

/*
var number = prompt("Add some number ");

function getFactorial(n) {
    var fac = 1;
    for (var i = 1; i <= n; i++) {
        fac = fac * i;
    }
    return fac;
}

document.write(number + '!:' + getFactorial(number));

*/